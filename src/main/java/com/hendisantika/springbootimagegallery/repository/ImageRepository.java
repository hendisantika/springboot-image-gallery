package com.hendisantika.springbootimagegallery.repository;

import com.hendisantika.springbootimagegallery.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-image-gallery
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-23
 * Time: 07:02
 */
public interface ImageRepository extends JpaRepository<Image, Long> {

    Image findByName(String text);

    Image findFirstByName(String text);

}