package com.hendisantika.springbootimagegallery.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-image-gallery
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-23
 * Time: 07:07
 */
public class SiteUserForm {
    private String name;

    private String password;

    public SiteUserForm() {
    }

    public SiteUserForm(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
